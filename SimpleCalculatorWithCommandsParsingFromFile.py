def SimpleCalculator(operation, A, B):
    if(operation == 'X' or operation == 'x'):
        return A*B
    elif(operation == '+'):
        return A+B
    elif(operation == '-'):
        return A-B
    elif(operation == '/'):
        return A/B
    else:
        return 'Invalid operation'


#print(SimpleCalculator("/", 6, 3))

LinesAlreadyJumpedTo = {}
gotoLineNumber = 1

with open("step_3.txt", 'r') as f:
    instructionlines = f.readlines()

    while gotoLineNumber not in LinesAlreadyJumpedTo:

        line = instructionlines[gotoLineNumber-1]

        #print(f'{gotoLineNumber} {line}')

        LinesAlreadyJumpedTo[gotoLineNumber] = line

        commandComponents = line.split(' ')

        if(commandComponents[1] == 'calc'):
            gotoLineNumber = int(SimpleCalculator(
                commandComponents[2], int(commandComponents[3]), int(commandComponents[4])))
        else:
            gotoLineNumber = int(commandComponents[1])

        line = instructionlines[gotoLineNumber]

print(f'{gotoLineNumber} {LinesAlreadyJumpedTo[gotoLineNumber]}')
