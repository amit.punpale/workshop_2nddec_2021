def SimpleCalculator(operation, A, B):
    if(operation == 'X' or operation == 'x'):
        return A*B
    elif(operation == '+'):
        return A+B
    elif(operation == '-'):
        return A-B
    elif(operation == '/'):
        return A/B
    else:
        return 'Invalid operation'


#print(SimpleCalculator("/", 6, 3))

finalresult = 0
with open("step_2.txt", 'r') as f:
    commandslines = f.readlines()
    for commandline in commandslines:
        # print(command)
        command, operation, A, B = commandline.split(' ')
        # print(commandline)
        #print(operation, A, B)
        result = SimpleCalculator(operation, int(A), int(B))
        finalresult += result

print(finalresult)
